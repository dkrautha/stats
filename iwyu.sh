#!/usr/bin/env bash

if [ "$1" == "check" ]; then
	iwyu-tool -p=./compile_commands.json | tee ./iwyu_output
fi
if [ "$1" == "fix" ]; then
	iwyu-fix-includes <./iwyu_output
fi
