#pragma once

#include <algorithm>
#include <cmath>
#include <complex>
#include <concepts>
#include <iterator>
#include <numeric>
#include <string_view>
#include <tl/expected.hpp>
#include <utility>

#include "types.hpp"

namespace dk::stats {

using namespace std::string_view_literals;
using dk::types::f64;
using std::accumulate;
using std::string_view;
using std::ranges::begin;
using std::ranges::end;
using tl::expected;
using tl::make_unexpected;

namespace concepts {

template <typename T>
concept Add = requires(T x, T y) {
  x + y;
  x += y;
};

template <typename T>
concept Sub = requires(T x, T y) {
  x - y;
  x -= y;
};

template <typename T>
concept Mul = requires(T x, T y) {
  x* y;
  x *= y;
};

template <typename T>
concept Div = requires(T x, T y) {
  x / y;
  x /= y;
};

template <typename T>
struct is_complex : std::false_type {};

template <std::floating_point T>
struct is_complex<std::complex<T>> : std::true_type {};

template <typename T, typename U>
concept explicitly_convertible_to = requires(T x) { static_cast<U>(x); };

template <typename T>
concept SupportedStatsType = Add<T> and Sub<T> and Mul<T> and Div<T> and
                             not is_complex<T>::value and explicitly_convertible_to<T, f64> and
                             std::default_initializable<T> and std::equality_comparable<T>;

template <typename T>
concept StatsRange =
    std::ranges::sized_range<T> and SupportedStatsType<std::ranges::range_value_t<T>>;

}  // namespace concepts

using concepts::StatsRange;

template <typename ReturnType = f64>
constexpr auto mean(const StatsRange auto& data) -> expected<ReturnType, string_view> {
  auto count = ReturnType{0};
  const auto fold_function = [&count](auto mean, const auto x) {
    mean += x;
    count += ReturnType{1};
    return mean;
  };
  const auto sum = accumulate(begin(data), end(data), ReturnType{}, fold_function);
  if (count == ReturnType{0}) {
    return make_unexpected("data has a size of zero"sv);
  }
  return sum / count;
}

constexpr auto geometric_mean(const StatsRange auto& data) -> expected<f64, string_view> {
  auto count = 0;
  const auto fold_function = [&count](auto gmean, const auto x) {
    const auto f64_x = static_cast<f64>(x);
    const auto log_x = std::log(f64_x);
    gmean += log_x;
    count += 1;
    return gmean;
  };
  const auto sum = accumulate(begin(data), end(data), 0.0, fold_function);
  if (count == 0) {
    return make_unexpected("data has a size of zero"sv);
  }
  return std::exp(sum / count);
}

}  // namespace dk::stats
